using UnityEngine;

public class saltoPlayer : MonoBehaviour
{
    public float jumpForce = 10f;
    public int maxJumps = 2;  

    private int jumpsLeft;
    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        jumpsLeft = maxJumps;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && jumpsLeft > 0)
        {
            Jump();
        }
    }

    private void Jump()
    {
        rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        jumpsLeft--;
    }

    private void OnCollisionEnter(Collision collision)
    {
        // Restablecer los saltos disponibles al tocar el suelo.
        if (collision.gameObject.CompareTag("Piso"))
        {
            jumpsLeft = maxJumps;
        }
    }
}

