using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arribaAbajo : MonoBehaviour
{

    bool tengoQueBajar = false;
    public float rapidez;

    public float topeArriba, topeAbajo;





    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y >= topeArriba)
        {
            tengoQueBajar = true;
        }
        if (transform.position.y <= topeAbajo)
        {
            tengoQueBajar = false;
        }
        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }


    }


    void Subir()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
    }


    void Bajar()
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
    }


}
