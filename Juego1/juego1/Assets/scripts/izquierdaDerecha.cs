using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class izquierdaDerecha : MonoBehaviour
{

    bool tengoQueIrDerecha = false;
    public float rapidez;

    public float topeIzquierda, topeDerecha;





    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x <= topeIzquierda)
        {
            tengoQueIrDerecha = true;
        }
        if (transform.position.x >= topeDerecha)
        {
            tengoQueIrDerecha = false;
        }
        if (tengoQueIrDerecha)
        {
            Derecha();
        }
        else
        {
            Izquierda();
        }


    }


    void Derecha()
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
    }


    void Izquierda()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
    }



}
