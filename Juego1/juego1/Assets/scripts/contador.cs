using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class contador : MonoBehaviour
{


    public float tiempoRestante = 60.0f;
    public Text textoContador;
    public Text perdiste;

    private void Update()
    {
        if (tiempoRestante > 0)
        {
            tiempoRestante -= Time.deltaTime;
            ActualizarTextoContador();
        }
        else
        {
           
            tiempoRestante = 0;
            perdiste.text = "Game Over, presiona R para reintentar...";
            ActualizarTextoContador();
            Time.timeScale = 0;
        }
    }

    void ActualizarTextoContador()
    {
        if (textoContador != null)
        {
            int segundos = Mathf.CeilToInt(tiempoRestante);
            textoContador.text = "Tiempo: " + segundos;
        }
    }
}
