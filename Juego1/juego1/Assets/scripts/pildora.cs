using UnityEngine;

public class pildora : MonoBehaviour
{
    public float aumentoDeEscala = 2.0f; // Factor de aumento de escala

    private bool aumentoActivo = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !aumentoActivo)
        {
            aumentoActivo = true;
            AumentarEscalaDelJugador(other.transform);
            Destroy(gameObject);
        }
    }

    private void AumentarEscalaDelJugador(Transform jugadorTransform)
    {
        jugadorTransform.localScale *= aumentoDeEscala;
        aumentoActivo = false;
    }
}