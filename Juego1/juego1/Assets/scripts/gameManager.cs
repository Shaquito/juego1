using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class gameManager : MonoBehaviour
{
    public string textValue;
    public Text lingotesObtenidos;
    public Text ganaste;
    private int score = 0;


    public Vector3 initialPlayerPosition;
    private GameObject player;

    public GameObject checkpoint;

    Vector3 spawnPoint;







    // Start is called before the first frame update
    void Start()
    {

        player = GameObject.FindGameObjectWithTag("Player");


        spawnPoint = gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {

        
        lingotesObtenidos.text = score.ToString();

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene("nivel1");
            Time.timeScale = 1;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Respawn"))
        {
            spawnPoint= checkpoint.transform.position;
            //Destroy(checkpoint);
        }

        if (other.gameObject.CompareTag("enemigo"))
        {


            player.transform.position = spawnPoint;



        }


        if (other.gameObject.CompareTag("coleccionable") == true)
        {
            score++;

        }

        if (other.gameObject.CompareTag("Finish") == true)
        {
            ganaste.text = "Ganaste!";
            Time.timeScale = 0;

        }



    }


  






}
